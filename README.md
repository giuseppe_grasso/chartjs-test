# chartjs-test

## -1- Project setup
```
npm install
```
## -2- Fix vulnerabilities, please execute:
```
npm audit fix
```

## -3- run the server
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```


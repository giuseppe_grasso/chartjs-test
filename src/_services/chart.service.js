import axios from 'axios'

//  for many endpoints depending on different environments should be stored in .env files
const chartApiUrl = 'https://api.coindesk.com/v1/bpi/historical/close.json?start=2019-01-01&end=2019-12-31'

export const chartService = {
	getChartData
}

function getChartData () {
	return fetchData(chartApiUrl)
}

function fetchData (apiUrl) {
	return axios({
		baseURL: apiUrl
	})
		.then(resp => {
			return { data: resp.data }
		})
		.catch(err => {
			if (err.response) {
				// if (err.response.status == 401) {
				//     userService.logout();
				//     return { ok: false, message: err.response.data.message }
				// }
				// if (err.response.status == 404) {
				//     return { ok: false, message: err.response.data.message }
				// }
				return err.response
				// handle other errors
			} else {
				return { message: 'Unexpected Error' }
			}
		})
}
